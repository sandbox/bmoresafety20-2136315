<?php

/**
 * @file
 * Contains the ZIP display plugin.
 */
class views_plugin_display_zip extends views_plugin_display_page {

  /**
   * Declare the type of styles this display is capable of utilizing.
   * 
   * @return string
   */
  function get_style_type() {
    return 'zip';
  }

  /**
   * Declare the options used by this display.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['filename'] = array('default' => 'download.zip');
    return $options;
  }

  /**
   * Create the form for setting options on this display.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'filename':
        $form['filename'] = array(
          '#type' => 'textfield',
          '#title' => t('Filename'),
          '#default_value' => $this->get_option('filename'),
          '#required' => TRUE,
        );
    }
  }

  /**
   * Provide the summary for page options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);
    $options['filename'] = array(
      'category' => 'page',
      'title' => t('Filename'),
      'value' => views_ui_truncate($this->get_option('filename'), 24),
    );
  }

  /**
   * Submit handler for the display's settings form.
   */
  function options_submit(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'filename':
        $this->set_option('filename', $form_state['values']['filename']);
        break;
    }
  }

  /**
   * Provide a listing of the files when they're previewed.
   */
  function preview() {
    $this->view->render();
    $args['items'] = array();
    foreach ($this->files() as $file) {
      $args['items'][] = l($file->filename, file_create_url($file->uri));
    }
    return t('Files included in this zip:') . theme('item_list', $args);
  }

  /**
   * Render the display.
   *
   * Here we analyze the files included in the result set, generate the ZIP file
   * that contains the output, and conduct the file transfer of the resulting
   * ZIP file.
   */
  function render() {
    if (!empty($this->view->preview)) {
      return;
    }

    // Generate the file if needed.
    if (!file_exists('public://' . $this->filename())) {
      $this->generateZip();
    }

    // Transfer the file.
    $headers['Content-Type'] = 'application/octet-stream';
    $headers['Content-Description'] = 'File Transfer';
    $headers['Content-Disposition'] = 'inline; filename=' . $this->get_option('filename');
    $headers['Content-Transfer-Encoding'] = 'binary';
    $headers['Content-Length'] = filesize('public://' . $this->filename());
    file_transfer('public://' . $this->filename(), $headers);
  }

  /**
   * Create the ZIP file on disk.
   */
  function generateZip() {
    $lib = libraries_load('pclzip');
    if (!$lib) {
      // Error.
      return;
    }

    $path = 'temporary://' . $this->filename();
    $dirname = drupal_dirname($path);
    file_prepare_directory($dirname, FILE_CREATE_DIRECTORY);
    $file = file_unmanaged_save_data('', $path);

    // Add in the files to be ZIP'd up.
    ini_set('max_execution_time', 240);
    $archive = new PclZip($file);
    foreach ($this->files() as $row) {
      $archive->add(drupal_realpath($row->uri), PCLZIP_OPT_REMOVE_ALL_PATH);
    }

    // Move into permanent storage.
    $path = 'public://' . $this->filename();
    $dirname = drupal_dirname($path);
    file_prepare_directory($dirname, FILE_CREATE_DIRECTORY);
    $file = file_unmanaged_move($file, $path, FILE_EXISTS_REPLACE);

    return $file;
  }

  /**
   * Generate the filename used for the cached ZIP file.
   */
  function filename() {
    static $filename;

    if (!$filename) {
      $filename = implode('+', $this->fids());
      $filename = md5($filename);
      $filename = "views_zip/{$filename}.zip";
    }

    return $filename;
  }

  /**
   * Create a list of fids for files to be included in the zip.
   */
  function fids() {
    static $fids;

    if ($fids === NULL) {
      $fids = array();
      foreach ($this->view->views_zip_files as $row) {
        $fids[] = $row['fid'];
      }
      sort($fids);
      $fids = array_keys($this->view->views_zip_files);
    }
    return $fids;
  }

  /**
   * Load the files to be included in the zip.
   */
  function files() {
    $files = file_load_multiple($this->fids());
    return $files;
  }

}
