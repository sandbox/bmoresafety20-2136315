<?php

/**
 * @file
 * Contains the ZIP row plugin.
 */
class views_plugin_row_zip_fields extends views_plugin_row {

  /**
   * Use the pre_prerender method to look for file fields on the results.
   */
  public function pre_render($result) {
    // Field types available for inclusion in ZIP.
    $field_types = array('file', 'image');

    // Itereate over fields configured on the View.
    foreach ($this->view->field as $alias => $field) {
      if (!empty($field->field_info['type']) && in_array($field->field_info['type'], $field_types)) {
        // Iterate over each row, looking for the field.
        foreach ($result as $row) {
          $values = $field->get_value($row, $alias);
          foreach ($values as $value) {
            $this->view->views_zip_files[$value['fid']] = $value;
          }
        }
      }
    }
  }

}
