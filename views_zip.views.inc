<?php

/**
 * @file
 * Views plugin handlers.
 */

/**
 * Implements hook_views_plugins().
 */
function views_zip_views_plugins() {
  $path = drupal_get_path('module', 'views_zip');

  $plugins['display']['zip'] = array(
    'title' => t('ZIP'),
    'help' => t('Display the view as replacement for an existing menu path.'),
    'path' => "$path/plugins",
    'handler' => 'views_plugin_display_zip',
    'theme' => 'views_view',
    'uses hook menu' => TRUE,
    'uses options' => TRUE,
    'admin' => t('ZIP'),
    'type' => 'zip',
  );

  $plugins['style']['zip'] = array(
    'title' => t('ZIP download'),
    'help' => t('Generates an ZIP file from a view.'),
    'handler' => 'views_plugin_style',
    'path' => "$path/plugins",
    'theme' => 'views_view_zip',
    'type' => 'zip',
    'uses row plugin' => TRUE,
  );

  $plugins['row']['zip_fields'] = array(
    'title' => t('Fields'),
    'help' => t('Include fields in a ZIP file.'),
    'handler' => 'views_plugin_row_zip_fields',
    'theme' => 'views_view_row_zip',
    'type' => 'zip',
    'uses fields' => TRUE,
  );

  return $plugins;
}
